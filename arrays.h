#ifndef ARRAYS_H
#define ARRAYS_H

#include <stdexcept>

struct indexer_1d
{
  int size;

  indexer_1d()
    : size(0) {};

  indexer_1d(int size)
    : size(size) {};

  indexer_1d(const indexer_1d& rhs)
    : size(rhs.size) {};

  indexer_1d& operator=(const indexer_1d& rhs)
  {
    size = rhs.size;
    return *this;
  }

  int operator()(int i)
  {
    return i;
  }
};

struct indexer_2d
{
  int size;
  int Nx;
  int Ny;

  indexer_2d()
    : size(0), Nx(0), Ny(0) {};

  indexer_2d(int Nx, int Ny)
    : Nx(Nx), Ny(Ny), size(Nx*Ny) {};

  indexer_2d(const indexer_2d& rhs)
    : Nx(rhs.Nx), Ny(rhs.Ny), size(rhs.size) {};

  indexer_2d& operator=(const indexer_2d& rhs)
  {
    size = rhs.size;
    Nx = rhs.Nx;
    Ny = rhs.Ny;
    return *this;
  }

  int operator()(int i, int j)
  {
    return Ny*i + j;
  }
};

struct indexer_3d
{
  int size;
  int Nx;
  int Ny;
  int Nz;

  indexer_3d()
    : size(0), Nx(0), Ny(0), Nz(0) {};

  indexer_3d(int Nx, int Ny, int Nz)
    : Nx(Nx), Ny(Ny), Nz(Nz), size(Nx*Ny*Nz) {};

  indexer_3d(const indexer_3d& rhs)
    : Nx(rhs.Nx), Ny(rhs.Ny), Nz(rhs.Nz), size(rhs.size) {};

  indexer_3d& operator=(const indexer_3d& rhs)
  {
    size = rhs.size;
    Nx = rhs.Nx;
    Ny = rhs.Ny;
    Nz = rhs.Nz;
    return *this;
  }

  int operator()(int i, int j, int k)
  {
    return Nz*(Ny*i + j) + k;
  }
};


template<class T, class I>
struct base_array
{
  T* __restrict__ data;
  I indexer;

  base_array()
    : data(0), indexer()
  {}

  template<typename... Args>
  base_array(Args... args)
  {
    init(args...);
  }

  base_array(const base_array<T,I>& arr)
    : indexer(arr.indexer)
  {
    data = new T[indexer.size];
    for (int i = 0; i < indexer.size; ++i)
      data[i] = arr.data[i];
  }

  base_array(base_array<T,I>&& arr)
    : data(arr.data), indexer(arr.indexer)
  {
    arr.data = nullptr;
  }


  base_array<T,I>& operator=(const base_array<T,I>& rhs)
  {
    if (this == &rhs)
      return *this;
    for (int i = 0; i < indexer.size; ++i)
      data[i] = rhs.data[i];
    return *this;
  }

  base_array<T,I>& operator=(base_array<T,I>&& rhs)
  {
    if (this == &rhs)
      return *this;

    data = rhs.data;
    indexer = rhs.indexer;
    rhs.data = nullptr;
    return *this;
  }


  template<typename... Args>
  void init(Args... args)
  {
    indexer = I(args...);
    data = new T[indexer.size];
  }

  void fill(T val)
  {
    for (int i = 0; i < indexer.size; ++i)
      data[i] = val;
  }

  ~base_array()
  {
    if (data)
      {
        delete[] data;
        data = 0;
      }
  }

  template<typename... Args>
  T& operator()(Args... args)
  {
    // if (!data)
    //   throw std::runtime_error("Uninitialized array");

    return data[indexer(args...)];
  }

  template<typename... Args>
  const T& operator()(Args... args) const
  {
    // if (!data)
    //   throw std::runtime_error("Uninitialized array");

    return data[indexer(args...)];
  }

};

typedef base_array<int, indexer_1d> int_1d;
typedef base_array<double, indexer_1d> double_1d;
typedef base_array<int, indexer_2d> int_2d;
typedef base_array<double, indexer_2d> double_2d;
typedef base_array<int, indexer_3d> int_3d;
typedef base_array<double, indexer_3d> double_3d;


#endif
