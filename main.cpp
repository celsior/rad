/*
  This program uses code of the 'smallpt' renderer
  (http://www.kevinbeason.com/smallpt/ (c) Kevin Beason, MIT Licence)
*/

#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <map>
#include <algorithm>
#include <string>
#include <unordered_set>
#include <functional>
#include <unordered_set>

#include <float.h>
#include <time.h>
#include "arrays.h"

std::numeric_limits<double> real;
const double infinity = real.infinity();

enum medium_type {MT_AP, MT_AL, MT_HTPB};
double absorption[] = {2.4e-6, 1e12, 3.6e-6};
const double eps = 1e-5;

int Nx = 200;
int Ny = 200;
int Nz = 200;


double_3d distr_matrix(Nx, Ny, Nz);
double_3d distr_al(Nx, Ny, Nz);

using std::istream;
using std::ostream;
using std::min;
using std::max;

double rnd(double mag = 1, double min = 0)
{
  return double(rand())/RAND_MAX * mag + min;
}

double sqr(double v)
{
  return v*v;
}

double cbk(double v)
{
  return v*v*v;
}

int sign(double v)
{
  return (v < 0 ? -1
          : v > 0 ? 1
          : 0);
}


double gauss(double mu = 0, double sigma = 1)
{
  return mu + sigma * sqrt(-2 * log(rnd())) * cos(2*M_PI*rnd());
}


struct vector
{
public:
  double x;
  double y;
  double z;

  vector(): x(0), y(0), z(0) {};
  vector(double x, double y, double z): x(x), y(y), z(z) {};
  vector(const vector& v): x(v.x), y(v.y), z(v.z) {};
  vector(istream& str);

  double length() const;
  double length2() const;
  vector orth() const;

  void operator+=(const vector& v);
};

vector operator*(const vector& v, const double s)
{
  return vector(v.x*s, v.y*s, v.z*s);
};

vector operator*(const double s, const vector& v)
{
  return vector(v.x*s, v.y*s, v.z*s);
};

vector operator/(const vector& v, const double s)
{
  return vector(v.x/s, v.y/s, v.z/s);
};

vector::vector(istream& str)
{
  str >> x >> y >> z;
}

double vector::length() const
{
  return sqrt(x*x + y*y + z*z);
}

double vector::length2() const
{
  return x*x + y*y + z*z;
}


vector vector::orth() const
{
  const double len = length();
  if (len != 0)
    return (*this)/len;
  else
    return vector(0, 0, 0);
}

ostream& operator<<(ostream& str, const vector& v)
{
  str << v.x << " " << v.y << " " << v.z;
  return str;
}

istream& operator>>(istream& str, vector& v)
{
  str >> v.x >> v.y >> v.z;
  return str;
}

void vector::operator+=(const vector& v)
{
  x += v.x;
  y += v.y;
  z += v.z;
}

bool operator==(const vector& v1, const vector& v2)
{
  return (v1.x == v2.x) && (v1.y == v2.y) && (v1.z == v2.z);
}

bool operator!=(const vector& v1, const vector& v2)
{
  return (v1.x != v2.x) || (v1.y != v2.y) || (v1.z != v2.z);
}


vector operator+(const vector& v1, const vector& v2)
{
  return vector(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z);
}

vector operator-(const vector& v1, const vector& v2)
{
  return vector(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z);
}

double dot(const vector &a, const vector &b)
{
  return a.x*b.x + a.y*b.y + a.z*b.z;
}

vector cross(const vector &a, const vector &b)
{
  return {a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x};
}

struct TraceStat
{
  double out_front;
  double out_back;
  double absorbed_matrix;
  double absorbed_al;
};


struct Ray
{
  double intensity;
  medium_type mt;
  vector origin;
  vector dir;

  void trace(TraceStat& stat);
};

vector min_point(0,0,0);
vector max_point(400,400,400);

double length = max_point.x - min_point.x;
double width = max_point.y - min_point.y;
double height = max_point.z - min_point.z;

double dx = (max_point.x - min_point.x)/Nx;
double dy = (max_point.y - min_point.y)/Ny;
double dz = (max_point.z - min_point.z)/Nz;


class flipper
{
public:
  const vector& center;
  const double radius;

  const vector& lo;
  const vector& hi;

  unsigned int n;
  unsigned int pat;
  vector tmp;

  enum {NF = 1, FX = 2, FY = 4, FZ = 8, FXY = 16, FXZ = 32, FXYZ = 64, FYZ = 128};

  flipper(const vector& center, const double radius, const vector& min_bound, const vector& max_bound)
    : center(center), radius(radius), lo(min_bound), hi(max_bound), n(1)
  {
    unsigned int fx = (center.x - radius < lo.x || center.x + radius > hi.x);
    unsigned int fy = (center.y - radius < lo.y || center.y + radius > hi.y);
    unsigned int fz = (center.z - radius < lo.z || center.z + radius > hi.z);

    pat = 1 + (fx<<1) + (fy<<2) + (fz<<3) + ((fx&&fy)<<4) + ((fx&&fz)<<5) + ((fx&&fy&&fz)<<6) + ((fy&&fz)<<7);
  };

  bool flip(double& v, double lo, double hi) const
  {
    if (v - radius < lo)
      {
        v = hi + (v - lo);
        return true;
      }
    else if (v + radius > hi)
      {
        v = lo - (hi - v);
        return true;
      }

    return false;
  }

  vector const* flip()
  {
    tmp = center;

    while (pat)
      {
        bool f = pat & n;
        if (f)
          {
            pat ^= n;
            switch (n)
              {
              case NF:
                break;
              case FX:
                flip(tmp.x, lo.x, hi.x); break;
              case FY:
                flip(tmp.y, lo.y, hi.y); break;
              case FZ:
                break;
                flip(tmp.z, lo.z, hi.z); break;
              case FXY:
                flip(tmp.x, lo.x, hi.x); flip(tmp.y, lo.z, hi.y); break;
              case FXZ:
                break;
                flip(tmp.x, lo.x, hi.x); flip(tmp.z, lo.z, hi.z); break;
              case FXYZ:
                break;
                flip(tmp.x, lo.x, hi.x); flip(tmp.y, lo.y, hi.y); flip(tmp.z, lo.z, hi.z); break;
              case FYZ:
                break;
                flip(tmp.y, lo.y, hi.y); flip(tmp.z, lo.z, hi.z); break;
              default:
                throw std::runtime_error("unreacheable");
              }
          }
        n <<= 1;
        if (f)
          return &tmp;
      }

    return 0;
  }
};

vector diffuse_reflection(const vector& normal)
{
  double r1 = 2*M_PI * (double)rand()/RAND_MAX;
  double r2 = (double)rand()/RAND_MAX;
  double r2s = sqrt(r2);
  vector w = normal;
  vector u = cross(fabs(w.x) > 0.1 ? vector{0,1,0} : vector{1,0,0},  w).orth();
  vector v = cross(w, u);
  return (u*cos(r1)*r2s + v*sin(r1)*r2s + w*sqrt(1-r2)).orth();
}

class Particle
{
public:
  int N;
  vector center;
  double R;
  medium_type mt;
  std::vector< std::pair<int,double> > cv;

  Particle(int N, vector center, double R, medium_type mt)
    : N(N), center(center), R(R), mt(mt) {};

  Particle(int N, istream& str)
    : N(N)
  {
    std::string tag;
    std::string distr;
    bool fixed;

    str >> tag >> distr >> center >> R >> fixed;
    if (tag == "AP")
      mt = MT_AP;
    else if (tag == "Al")
      mt = MT_AL;
    else
      throw std::runtime_error("Unknown Particle tag " + tag);
  }

  vector min_bb_vector() const
  {
    return vector(center.x-R, center.y-R, center.z-R);
  }

  vector max_bb_vector() const
  {
    return vector(center.x+R, center.y+R, center.z+R);
  }

  double ray_sphere(const Ray &ray) const
  {
    vector op = center - ray.origin;
    double b = dot(op, ray.dir);
    double det = b*b - dot(op,op) + R*R;
    if (det < 0)
      return infinity;

    det = sqrt(det);

    if (b - det > 1e-6)
      return b - det;
    if (b + det > 1e-6)
      return b + det;

    return infinity;
  }

  vector intersect(const Ray &ray) const
  {
    double lambda = ray_sphere(ray);
    return (ray.origin + lambda*ray.dir - center).orth();
  }

  vector diffuse_reflection(const Ray &ray)
  {
    double lambda = ray_sphere(ray);
    vector intr_pt = ray.origin + lambda*ray.dir;
    vector norm = (intr_pt - center).orth();
    vector nl = dot(norm, ray.dir) < 0 ? norm : norm*-1;

    return ::diffuse_reflection(nl);
  }

  vector specular_reflection(const Ray &ray)
  {
    double lambda = ray_sphere(ray);
    vector intr_pt = ray.origin + lambda*ray.dir;
    vector norm = (intr_pt - center).orth();
    norm = dot(norm, ray.dir) > 0 ? norm : norm*-1;

    return norm * 2 * dot(norm, ray.dir*-1) + ray.dir;
  }
};

double analyze_cell(const Particle& p, int x, int y, int z)
{
  bool in = false;
  bool out = false;

  for (int i = 0; i <= 1; ++i)
    for (int j = 0; j <= 1; ++j)
      for (int k = 0; k <= 1; ++k)
        {
          vector probe{(x+i)*dx, (y+j)*dy, (z+k)*dz};
          if ( (probe - p.center).length() < p.R )
            in = in || true;
          else
            out = out || true;
        }
  if (in && !out)
    return 1;
  if (out && !in)
    return 0;

  const int N = 1e2;
  int hits = 0;
  for (int n = 0; n < N; ++n)
    {
      const vector pt(rnd(dx, x*dx),
                      rnd(dy, y*dy),
                      rnd(dz, z*dz));

      if ((p.center - pt).length() < p.R)
        ++hits;
    }

  return double(hits)/N;
}


void analyze_particle(Particle& p)
{
  flipper f1(p.center, p.R, min_point, max_point);
  double s = 0;
  while (const vector* p1 = f1.flip())
    {
      const int x_min = (int)floor((p1->x - p.R) / dx);
      const int x_max = (int)floor((p1->x + p.R) / dx);
      const int y_min = (int)floor((p1->y - p.R) / dy);
      const int y_max = (int)floor((p1->y + p.R) / dy);
      const int z_min = (int)floor((p1->z - p.R) / dz);
      const int z_max = (int)floor((p1->z + p.R) / dz);

      for (int x = std::max(x_min, 0); x <= std::min(x_max, Nx-1); ++x)
        for (int y = std::max(y_min, 0); y <= std::min(y_max, Ny-1); ++y)
          for (int z = std::max(z_min, 0); z <= std::min(z_max, Nz-1); ++z)
            {
              double v = analyze_cell(p, x, y, z);
              s += v;
              if (v > 0)
                {
                  if (distr_al.indexer(x,y,z) > distr_al.indexer.size)
                    {
                      std::cerr << x << " " << y << " " << z << " " << distr_al.indexer(x,y,z) << " | " << p.center << "\n";
                      throw "!";
                    }
                  p.cv.emplace_back(distr_al.indexer(x,y,z), v);
                }
            }
    }

  for (auto& v : p.cv)
    v.second /= s;
}

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/geometries/segment.hpp>
#include <boost/geometry/index/rtree.hpp>
#include <boost/geometry/geometries/register/point.hpp>
BOOST_GEOMETRY_REGISTER_POINT_3D(vector, double, cs::cartesian, x, y, z);
typedef boost::geometry::model::box<vector> box;
typedef boost::geometry::model::segment<vector> segment;
namespace bgi = boost::geometry::index;
typedef std::pair<box, Particle*> value;

class rtree_index
{
public:
  typedef bgi::quadratic<16> bgi_alg;
  bgi::rtree< value, bgi_alg > rtree;

  void add(Particle* p)
  {
    flipper f1(p->center, p->R, min_point, max_point);
    while (const vector* p1 = f1.flip())
      {
        box b = box(vector(p1->x - p->R, p1->y - p->R, p1->z - p->R),
                    vector(p1->x + p->R, p1->y + p->R, p1->z + p->R));
        rtree.insert(std::make_pair(b, p));
      }
  }

  void rem(Particle* p)
  {
    flipper f1(p->center, p->R, min_point, max_point);
    while (const vector* p1 = f1.flip())
      {
        box b = box(vector(p1->x - p->R, p1->y - p->R, p1->z - p->R),
                    vector(p1->x + p->R, p1->y + p->R, p1->z + p->R));
        rtree.remove(std::make_pair(b, p));
      }
  }

  Particle* intersect(const Ray& r, const vector& end, double& min_dist)
  {
    auto s = segment(r.origin, r.origin+r.dir*100);
    Particle* ret = nullptr;
    for (auto& v : rtree | bgi::adaptors::queried(bgi::intersects(s)))
      {
        double d = v.second->ray_sphere(r);
        if (min_dist > 0 && d <= min_dist)
          {
            min_dist = d;
            ret = v.second;
          }
      }
    return ret;
  }
};


typedef std::vector<Particle*> ParticleList;
rtree_index sp_index;
ParticleList all_particles;

double nround(double v)
{
  const double p = 1e-6;
  return round(v/p)*p;
}

vector nround(const vector& v)
{
  return {nround(v.x), nround(v.y), nround(v.z)};
}

void Ray::trace(TraceStat& stat)
{
  for(;;)
    {
      int nx = round(origin.x/dx);
      int ny = round(origin.y/dy);
      int nz = round(origin.z/dz);

      double next_x = (nx + sign(dir.x))*dx;
      next_x = std::min(std::max(next_x, 0.0), length);
      vector v1(next_x,
                std::min(std::max(origin.y + (next_x-origin.x)/dir.x*dir.y, 0.0), width),
                std::min(std::max(origin.z + (next_x-origin.x)/dir.x*dir.z, 0.0), height));
      double dv1 = (origin - v1).length();

      double next_y = (ny + sign(dir.y))*dy;
      next_y = std::min(std::max(next_y, 0.0), width);
      vector v2(std::min(std::max(origin.x + (next_y-origin.y)/dir.y*dir.x, 0.0), length),
                next_y,
                std::min(std::max(origin.z + (next_y-origin.y)/dir.y*dir.z, 0.0), height));
      double dv2 = (origin - v2).length();

      double next_z = (nz + sign(dir.z))*dz;
      next_z = std::min(std::max(next_z, 0.0), height);
      vector v3(std::min(std::max(origin.x + (next_z-origin.z)/dir.z*dir.x, 0.0), length),
                std::min(std::max(origin.y + (next_z-origin.z)/dir.z*dir.y, 0.0), width),
                next_z);
      double dv3 = (origin - v3).length();

      vector& nearest_wall = v1;
      double min_dist = dv1;
      if (dv2 < min_dist)
        {
          nearest_wall = v2;
          min_dist = dv2;
        }
      if (dv3 < min_dist)
        {
          nearest_wall = v3;
          min_dist = dv3;
        }

      vector new_dir = dir;
      medium_type new_mt = mt;

      Particle* nearest_particle = sp_index.intersect(*this, nearest_wall, min_dist);
      vector new_origin = origin + dir*min_dist;
      if (new_origin.x < 0)
        new_origin.x = 0;
      if (new_origin.y < 0)
        new_origin.y = 0;
      if (new_origin.z < 0)
        new_origin.z = 0;

      if (nearest_particle)
        {
          if (nearest_particle->mt == MT_AL)
            new_dir = nearest_particle->diffuse_reflection(*this);
          else if (mt == MT_HTPB)
            new_mt = nearest_particle->mt;
          else
            new_mt = MT_HTPB;
        }
      else if (new_origin.y < 1e-10)
        new_origin.y = width-1e-10;
      else if (new_origin.y > width-1e-10)
        new_origin.y = 1e-10;
      else if (new_origin.x < 1e-10)
        new_origin.x = length-1e-10;
      else if (new_origin.x > length-1e-10)
        new_origin.x = 1e-10;

      int idx_x = floor((origin.x + new_origin.x)/2/dx);
      int idx_y = floor((origin.y + new_origin.y)/2/dy);
      int idx_z = floor((origin.z + new_origin.z)/2/dz);

      if (idx_x == Nx)
        idx_x = Nx-1;
      if (idx_y == Ny)
        idx_y = Ny-1;
      if (idx_z == Nz)
        idx_z = Nz-1;

      double di = -intensity*expm1(-absorption[mt]*(origin - new_origin).length());
      distr_matrix(idx_x, idx_y, idx_z) += di;
      intensity -= di;
      stat.absorbed_matrix += di;

      if (intensity < eps)
        {
          //std::cerr << "DEC " << idx_x << " " << idx_y << " " << idx_z << "\n";
          distr_matrix(idx_x, idx_y, idx_z) += intensity;
          stat.absorbed_matrix += intensity;
          return;
        }

      if (nearest_particle && nearest_particle->mt == MT_AL)
        {
          for (auto& v : nearest_particle->cv)
            distr_al.data[v.first] += intensity*0.3*v.second;
          stat.absorbed_al += intensity*0.3;
          intensity -= intensity*0.3;
        }

      if (new_origin.z < 1e-10)
        {
          new_dir = diffuse_reflection({0,0,1});
          //std::cerr << "OUT " << new_origin << " I = " << intensity << "\n";
          //stat.out_front += intensity;
          //return;
        }
      else if (new_origin.z >= height - 1e-10)
        {
          //std::cerr << "OUT " << new_origin << " I = " << intensity << "\n";
          stat.out_back += intensity;
          return;
        }

      //std::cerr << "(" << origin << ") (" << dir << ") " << mt << " I = " << intensity << "\n";

      origin = new_origin;
      dir = new_dir;
      mt = new_mt;
    }
}


void read_particles(const char* fname)
{
  std::ifstream str(fname);
  if (! str.is_open())
    throw std::runtime_error("Cannot open file");

  double Vtotal = 0;
  char rectype = 0;
  int N = 0;
  str >> rectype;
  while (!str.eof() && (rectype != 'E') )
    {
      std::cerr << N << "\n";
      if (rectype == 'E')
        break;
      else if (rectype != 'P')
        throw std::runtime_error("Malformed file");;

      Particle* p = new Particle(N++, str);
      all_particles.push_back(p);
      sp_index.add(p);
      if (p->mt == MT_AL)
        analyze_particle(*p);
      Vtotal += 4.0/3*M_PI*pow(p->R, 3);

      str >> rectype;
    }

  double V0 = ((max_point.x-min_point.x) * (max_point.y-min_point.y) * (max_point.z-min_point.z));
  std::cerr << "READ " << all_particles.size() << " " << Vtotal / V0 << "\n";
}

int main(int argc, char** argv)
{
  srand(time(0));

  read_particles(argv[1]);

  std::ofstream out(argv[2]);

  int Nrays = 100;

  TraceStat stat{0,0,0,0};
  for (int i = 0; i < Nx; ++i)
    for (int j = 0; j < Ny; ++j)
      {
        std::cerr << i << " " << j << "\n";
        for (int n = 0; n < Nrays; ++n)
          {
            double phi = (double)rand()/RAND_MAX * M_PI;
            double th = (double)rand()/RAND_MAX * M_PI;
            double x = (double)rand()/RAND_MAX*dx;
            double y = (double)rand()/RAND_MAX*dy;

            Ray r{1, MT_HTPB, vector{i*dx+x, j*dy+y, 0}, vector{cos(phi)*sin(th), cos(th), sin(phi)*sin(th)}};
            r.trace(stat);
          }
      }

  for (int i = 0; i < Nx; ++i)
    for (int j = 0; j < Ny; ++j)
      for (int k = 0; k < Nz; ++k)
        out << i << " " << j << " " << k << " " << distr_matrix(i,j,k)/Nrays/Nx/Ny << " " << distr_al(i,j,k)/Nrays/Nx/Ny << "\n";

  std::cerr << stat.out_front/Nrays/Nx/Ny << " "
            << stat.out_back/Nrays/Nx/Ny << " "
            << stat.absorbed_matrix/Nrays/Nx/Ny << " "
            << stat.absorbed_al/Nrays/Nx/Ny << "\n";

  return 0;
}
